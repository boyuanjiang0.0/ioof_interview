var express = require('express');
var router = express.Router();
const path = require('path')
var docClient = require("../dbService");

/* GET home page. */
router.get('/', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../pages/meta.html'))
});

// save api 
router.post("/api/submit", async (req, res) => {
  req.body.id = new Date().getTime();
  console.log("this is body", req.body);
  var params = {
    TableName: "interview",
    Item: req.body
  }
  const result = docClient.put(params, function (err, data) {
    if (err) {
      console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
      return res.status(200).json({
        success: false,
        data: err,
      });
    } else {
      console.log("Added item:");
      return res.status(200).json({
        success: true,
        data: "intser success",
      });
    }
  });
  return result
});

// get api
router.get("/api/fetch", async (req, res) => {
  const result = await docClient.scan({
    TableName: "interview",
  })
    .promise()
    .then(result => {
      return res.status(200).json({
        success: true,
        data: result,
      });
    })
    .catch((err) => {
      return res.status(200).json({
        success: false,
        data: err,
      });
    })
  return result;

});

module.exports = router;
